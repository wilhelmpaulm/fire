<!--  Start of Coding -->
<?php
//Begin Initlization
//Connect to DB
$servername = "localhost";
$username = "root";
$password = "";

//Create Connection
$conn = mysqli_connect($servername, $username, $password, "bfpmaindb1");
//$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

//Retrieve ID
$id = null;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}


//Select Data
if ($id != null) {
    mysqli_query($conn, "DELETE FROM `table1` WHERE `name`='$id' LIMIT 1");
} else {
    $sql = "SELECT * FROM table1";
    $result = mysqli_query($conn, $sql);
}
$sql = "SELECT * FROM table1";
$result = mysqli_query($conn, $sql);

?>
<!--  End of Coding -->




<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <!--            <div class="row placeholders">
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                        </div>-->

            <h2 class="sub-header">Section title</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>                 
                            <th>Name</th>                           
                            <th>Email</th>
                            <th>Website</th>
                            <th>Comments</th>
<!--                            <th>Middle Name</th>-->
                        </tr>                        
                    </thead>
                    <tbody>
                        <?php foreach ($result as $r) { ?>      
                            <tr>                                
                                <td><a href="?id=<?= $r['name'] ?>"><?= $r['name']; ?> </a></td> 
                                <td><?= $r['email']; ?></td>
                                <td><?= $r['website']; ?></td>
                                <td><?= $r['comment']; ?></td>
    <!--                                <td><//?= $r['middle_name']; ?></td>-->
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php include_once './view/template/footer.php'; ?>