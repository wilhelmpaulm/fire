
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Report a Fire</h1>


<!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-9">
                    <!--  MISSING "NAME" PARAMETER -->
                    <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <div class="form-group">
                            <label for="addressLocation">Incident Location<span class="c-red"> *</span></label>
                            <p class="help-block">Please be <span class="c-red">SPECIFIC</span> as possible</p>
                            <input type="text" class="form-control" id="addressLocation" placeholder="Blk 7. Village Name Street Name." autofocus="true">
                        </div>
<!--                            <p> Barangay, City Region </p>-->
                        <div class="row">
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label for="region">Region<span class="c-red"> *</span></label>
                                    <select class="form-control" name="region" id="region" disabled="TRUE">
                                        <option>NCR</option>
                                        <option>Region 1</option>
                                        <option>Region 2</option>
                                        <option>Region 3</option>
                                        <option>Region 4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label for="city">City<span class="c-red"> *</span></label>
                                    <select class="form-control" name="city" id="city" disabled="true">
                                        <option>Manila</option>
                                        <option>Muntinlupa</option>
                                        <option>Quezon</option>
                                        <option>Las Pinas</option>
                                        <option>Makati</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label for="barangay">Barangay<span class="c-red"> *</span></label>
                                    <select class="form-control" name="barangay" id="barangay" disabled="true">
                                        <option>Malate</option>
                                        <option>Putatan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="reporter_last_name">Reporter's Last Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="reporter_last_name" placeholder="Cruz">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="reporter_first_name">Reporter's First Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="reporter_first_name" placeholder="Robert">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="reporter_middle_name">Reporter's Middle Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="reporter_middle_name" placeholder="De La">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="reporter_contact">Reporter's Contact Number<span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="reporter_contact" placeholder="XXX-XXX-XXXX">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reporter_email">Reporter's Email<span class="c-red"> *</span></label>
                                    <input type="email" class="form-control" id="reporter_email" placeholder="me@here.com">
                                <!--    <textarea class="form-control" rows="3"></textarea>-->
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="reporter_email">Do you know the Owner?<span class="c-red"> *</span></label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Yes">Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">No
                            </label>
                        </div>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                        <input class="btn btn-primary pull-right" type="submit" name="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once './view/template/footer.php'; ?>