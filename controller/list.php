
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">List of ALL Fire Reports</h1>
        <!-- WE NEED A DATE SELECTOR FEATURE-->

            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-5">
<!--                    <table class="table table-hover">
                         On rows 
                        <tr class="active">Currently Fighting</tr>
                        <tr class="success">Fire Out</tr>
                        <tr class="warning">Pending Confirmation</tr>
                        <tr class="danger">IF IGNORED</tr>
                        <tr class="info">Information</tr>

                         On cells (`td` or `th`) 
                        <tr>
                            <td class="active">...</td>
                            <td class="success">...</td>
                            <td class="warning">...</td>
                            <td class="danger">...</td>
                            <td class="info">...</td>
                        </tr>
                    </table>-->
<!--                    <label>Pending, Active, and On-Going Fire/s</label><br>-->
                    <span class="text-muted c-black" style="font-size:x-small;">Legend:</span>
                    <span class="label label-danger">Ignored</span>
                    <span class="label label-warning">For Confirmation</span>
                    <span class="label label-success">In Progress</span>                    
                    <span class="label label-info">Confirmed</span>
                    <span class="label label-default">Fire Out</span><br>
                    <span class="text-muted c-red" style="font-size:x-small;">Click on the row for additional information</span>
                    <br>
                    <br>
                    <table class="table table-bordered table-hover table-responsive">
                        <thead>
                            <tr>
                                <th><i class="fa fa-clock-o"></i> Fire ID</th>
                                <th><i class="fa fa-map-marker"></i> Location (City)</th>
                                <th>DateTime</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="active">
                                <td class="time">12:00</td>
                                <td>Manila</td>
                                <td>{BLANK}</td>
                                <td>{BLANK}</td>
                            </tr>
                            <tr class="danger">
                                <td class="time">08:00</td>
                                <td>QC</td>
                                <td>{BLANK}</td>
                                <td>{BLANK}</td>
                            </tr>
                            <tr class="info">
                                <td class="time">12:30</td>
                                <td>Manila</td>
                                <td>{BLANK}</td>
                                <td>{BLANK}</td>
                            </tr>
                            <tr class="success">
                                <td class="time">05:00</td>
                                <td>Manila</td>
                                <td>{BLANK}</td>
                                <td>{BLANK}</td>
                            </tr>
                            <tr class="active">
                                <td class="time">12:00</td>
                                <td>Manila</td>
                                <td>{BLANK}</td>
                                <td>{BLANK}</td>
                            </tr>
                            
                        </tbody>
                    </table>


                </div>
                <div class="col-md-7">
                    <legend>Detail Information</legend>
                    <div class="well">
                        <div id="r-info">
                            <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                                <div class="row">
                                    <div class="col-lg-5">                             
                                        <div class="form-group">
                                            <label>Confirmation Status</label>
                                            <input type="text" class="form-control" placeholder="Confirmed" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">                             
                                        <div class="form-group">
                                            <label>DateTime</label>
                                            <input type="datetime" class="form-control" placeholder="31 December 2014 08:30:00" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Reporter Last Name</label>
                                            <input type="text" class="form-control" placeholder="Cruz" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Reporter First Name</label>
                                            <input type="text" class="form-control" placeholder="Juan" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Reporter Middle Name</label>
                                            <input type="text" class="form-control" placeholder="De La" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">                             
                                        <div class="form-group">
                                            <label>Fire ID</label>
                                            <input type="integer" class="form-control" placeholder="123456789" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">                             
                                        <div class="form-group">
                                            <label>Alarm Status</label>
                                            <input type="text" class="form-control" placeholder="3th Alarm" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">                             
                                        <div class="form-group">
                                            <label>Exact Address Location</label>
                                            <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. City, Region" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Owner's Last Name</label>
                                            <input type="text" class="form-control" placeholder="Cruz" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Owner's First Name</label>
                                            <input type="text" class="form-control" placeholder="Juan" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Owner's Middle Name</label>
                                            <input type="text" class="form-control" placeholder="De La" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <input class="btn btn-primary pull-right" type="button" name="view" value="  View  ">
                                </div>
                            </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('tr').on("click", function () {
//        alert();
//        $("#r-info").replaceWith($("#r-info-c"));
        var time = $(this).children(".time").text();
        $("#r-info").text(time);
    });
    
    
//    $('table').dataTable();
</script>

<?php include_once './view/template/footer.php'; ?>