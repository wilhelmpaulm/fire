
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"></h1>
            <h1 class="page-header">Fire Recovery Page</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <!--                        <div class="form-group">
                                                    <label for="addressLocation">Incident Location<span class="c-red"> *</span></label>
                                                    <p class="help-block">Please be <span class="c-red">SPECIFIC</span> as possible</p>
                                                    <input type="text" class="form-control" id="addressLocation" placeholder="Blk 7. Village Name Street Name." autofocus="true">
                                                </div>-->
                        <!--                            <p> Barangay, City Region </p>-->
                        <div class="row">
                            <!-- Using (Alt + 255) Character Space-->
                            <h4 class="sub-header">   Identifiers</h4>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Fire ID</label>
                                    <input type="integer" class="form-control" placeholder="123456789" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-8">                             
                                <div class="form-group">
                                    <label>Incident Location</label>
                                    <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Reported</label>
                                    <input type="datetime" class="form-control" placeholder="31 December 2014 18:30:00" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Extinguished</label>
                                    <input type="datetime" class="form-control" placeholder="31 December 2014 20:30:00" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Building Structure</label>
                                    <input type="text" class="form-control" placeholder="Residential" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Additional Information/s</h4>
                            
                        </div>
                        <div class="row">
                        <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="recovery_injuries">Any Additional Injuries?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="hidden"> <!--If Yes, Drop down an ADDITIONAL FIELDS of Casualties Information --></div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="recovery_casualties">Any Additional Casualties?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions" id="casualtiesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions" id="casualtiesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="hidden"> <!--If Yes, Drop down an ADDITIONAL FIELDS of Casualties Information --></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="recovery_insurance">Is the Building insured?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="insuranceRadioOptions" id="insuranceRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="insuranceRadioOptions" id="insuranceRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="hidden"> <!--If Yes, Drop down an ADDITIONAL FIELDS of Casualties Information --></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="recovery_damages">Estimated Amount of Damages <span class="text-muted c-red" style="font-size:x-small ">in PHP</span><span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="recovery_damages" placeholder="500000" autofocus="true">                                
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="recovery_families">Estimated Number of Families Affected <span class="c-red"> *</span></label>
                                    <input type="number" class="form-control" id="recovery_families" placeholder="50">                                
                                </div>
                            </div>
                        </div>
                        <br>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="    Revert    ">                            
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submit" value="    Submit    ">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once './view/template/footer.php'; ?>