
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Confirmation of Fire and Pre-Deployment</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-5">
<!--                    <table class="table table-hover">
                         On rows 
                        <tr class="active">Currently Fighting</tr>
                        <tr class="success">Fire Out</tr>
                        <tr class="warning">Pending Confirmation</tr>
                        <tr class="danger">IF IGNORED</tr>
                        <tr class="info">Information</tr>

                         On cells (`td` or `th`) 
                        <tr>
                            <td class="active">...</td>
                            <td class="success">...</td>
                            <td class="warning">...</td>
                            <td class="danger">...</td>
                            <td class="info">...</td>
                        </tr>
                    </table>-->
                    <label>Pending, Active, and On-Going Fire/s</label><br>
                    <span class="text-muted c-black" style="font-size:x-small;">Legend:</span>
                    <span class="label label-danger">Ignored</span>
                    <span class="label label-warning">For Confirmation</span>
                    <span class="label label-success">In Progress</span>                    
                    <span class="label label-info">Confirmed</span>
                    <span class="label label-default">Fire Out</span><br>
                    <span class="text-muted c-red" style="font-size:x-small;">Click on the row for additional information</span>
                    <br>
                    <br>
                    <table class="table table-bordered table-hover table-responsive">
                        <thead>
                            <tr>
                                <th><i class="fa fa-clock-o"></i> Time</th>
                                <th><i class="fa fa-map-marker"></i> Location (City)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="active">
                                <td class="time">12:00</td>
                                <td>Manila</td>
                            </tr>
                            <tr class="danger">
                                <td class="time">08:00</td>
                                <td>QC</td>
                            </tr>
                            <tr class="info">
                                <td class="time">12:30</td>
                                <td>Manila</td>
                            </tr>
                            <tr class="success">
                                <td class="time">05:00</td>
                                <td>Manila</td>
                            </tr>
                            <tr class="active">
                                <td class="time">12:00</td>
                                <td>Manila</td>
                            </tr>

                        </tbody>
                    </table>


                </div>
                <div class="col-md-7">
                    <legend>Report Information</legend>
                    <div class="well">
                        <div id="r-info">
                            <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                                <div class="row">
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Reporter Last Name</label>
                                            <input type="text" class="form-control" placeholder="Cruz" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Reporter First Name</label>
                                            <input type="text" class="form-control" placeholder="Juan" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">                             
                                        <div class="form-group">
                                            <label>Reporter Middle Name</label>
                                            <input type="text" class="form-control" placeholder="De La" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">                             
                                        <div class="form-group">
                                            <label>Exact Address Location</label>
                                            <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. City, Region" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">                             
                                        <div class="form-group">
                                            <label>Reporter Contact Number</label>
                                            <input type="integer" class="form-control" placeholder="XXX-XXX-XXXX" disabled="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">                             
                                        <div class="form-group">
                                            <label>Reporter E-Mail Address</label>
                                            <input type="email" class="form-control" placeholder="me@here.com" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="pull-right">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">
                                            <i class="glyphicon glyphicon-remove"></i> Decline
                                        </button>
                                        <button type="submit" class="btn btn-success btn-lg"><i class="glyphicon glyphicon-ok"></i> Confirm</button>
                                    </div>
                                </div>
                            </form>
<!--                            <p>Reporter Last Name: Cruz</p>
                            <p>Reporter First Name: Juan</p>
                            <p>Location: 123 Address St. Barangay, City</p>
                            
                            
                            <br>
                            <br>
                            <br>
                            <div class="btn-group btn-group-sm ">
                                <button class="btn btn-success">Confirm</button>
                                <button class="btn btn-danger">Deny</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirm Decline?</h4>
            </div>
            <form action="" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">                             
                            <div class="form-group">
                                <label for="confirm_decline_options">Reason:<span class="c-red"> *</span></label>
                                <select class="form-control" name="confirm_decline" id="confirm_decline_options">
                                    <option >Please Select One!</option>
                                    <option>Already Reported</option>
                                    <option>False Information</option>
                                    <option>Incomplete Information</option>
                                    <option>Intentional Misleading</option>
                                    <option>Prank</option>
                                    <option>Others</option>
                                </select>

                            </div>
                            <br>
                            <div class="form-group " id="rd">
                                <label for="reason_decline">Please Specify:<span class="c-red"> *</span></label>
                                <input type="text" class="form-control" name="reason_decline" required="" id="reason_decline" value=" " placeholder="Why?"/>
                            </div>
                        </div>
                    </div>
                     <p><span class="col-lg-offset-2 c-red fs20"> * </span> -- required fields</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="rdc" class="btn btn-warning" >Clear</button>
                    <button type="submit" id="rds" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $("#rd").hide();
    $("#rds").hide();
    $("#rdc").hide();

    $('tr').on("click", function () {
//        alert();
//        $("#r-info").replaceWith($("#r-info-c"));
        var time = $(this).children(".time").text();
        $("#r-info").text(time);
    });
    
    
    //shows or removes the option in reason for declining (Others)
    $('#confirm_decline_options').on("change", function () {
        var value = $(this).val();
        if (value == 'Others') {
            $("#rd").show();
            $("#rdc").show();
            
        } else {
            $("#rd").hide();
            $("#rdc").hide();
        }
    });
    
    //shows or removes the option in reason for declining (Others)
    $('#confirm_decline_options').on("change", function () {
        var value = $(this).val();
        if (value == 'Others') {
            $("#rd").show();
            $("#reason_decline").val("").focus();
        } else {
            $("#reason_decline").val(" ");
            $("#rd").hide();
        }
        if (value != 'Please Select One!') {
            $("#rds").show();
        } else {
            $("#rds").hide();
        }
    });
    
    $("#rdc").on("click", function(){
       $("#reason_decline").val(""); 
    });


//    $('table').dataTable();
</script>   

<?php include_once './view/template/footer.php'; ?>