
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"></h1>
            <h1 class="page-header">Risk-Assessment Page</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <!--                        <div class="form-group">
                                                    <label for="addressLocation">Incident Location<span class="c-red"> *</span></label>
                                                    <p class="help-block">Please be <span class="c-red">SPECIFIC</span> as possible</p>
                                                    <input type="text" class="form-control" id="addressLocation" placeholder="Blk 7. Village Name Street Name." autofocus="true">
                                                </div>-->
                        <!--                            <p> Barangay, City Region </p>-->
                        <div class="row">
                            <!-- Using (Alt + 255) Character Space-->
                            <h4 class="sub-header">   Identifiers</h4>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Fire ID</label>
                                    <input type="integer" class="form-control" placeholder="123456789" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-8">                             
                                <div class="form-group">
                                    <label>Incident Location</label>
                                    <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Alarm Status</label>
                                    <input type="text" class="form-control" placeholder="5th Alarm" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Reported</label>
                                    <input type="datetime" class="form-control" placeholder="31 December 2014 18:30:00" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Confirmed By</label>
                                    <input type="text" class="form-control" placeholder="Mario and Luigi" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Critical Information/s</h4>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_oic">Current Officer-In-Charge<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="assessment_oic" placeholder="Cruz, Joseph De La" autofocus="true">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_bldg_structure">Building Structure<span class="c-red"> *</span></label>
                                    <select class="form-control" name="building_structure" id="assessment_bldg_structure">
                                        <option>Non-Residential</option>
                                        <option>Residential</option>
                                        <option>Commercial</option>
                                        <option>Educational</option>
                                        <option>Government</option>
                                        <option>Industrial</option>
                                        <option>Agricultural</option>
                                        <option>Religious</option>
                                        <option>Parking/Storage</option>
                                        <option>Transportaion</option>
                                        <option>Infrastructure</option>
                                        <option>Power Stations</option>
                                        <option>Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="assessment_bldg_elevation">Elevation<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="assessment_bldg_elevation" placeholder="What Floor?">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_alarm_status">Alarm Status<span class="c-red"> *</span></label>
                                    <select class="form-control" name="alarm_status" id="assessment_alarm_status">
                                        <!--<option>NOT APPLICABLE</option>-->
                                        <option>5th-Alarm</option>
                                        <option>4th-Alarm</option>
                                        <option>3rd-Alarm</option>
                                        <option>2nd-Alarm</option>
                                        <option>1st-Alarm</option>
                                        <option>Task Force Alpha</option>
                                        <option>Task Force Bravo</option>
                                        <option>Task Force Charlie</option>
                                        <option>Task Force Delta</option>
                                        <option>Task Force Echo</option>
                                        <option>General Alarm</option>
                                    </select>                          
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_wind_speed">Wind Speed <span class="text-muted c-red" style="font-size:x-small ">in kph</span><span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="assessment_wind_speed" placeholder="6 kph">                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="assessment_wind_direction">Wind Direction<span class="c-red"> *</span></label>
                                    <select class="form-control" name="wind_direction" id="assessment_wind_direction">
                                        <option>North</option>
                                        <option>North-North-East</option>
                                        <option>North East</option>
                                        <option>East-North-East</option>
                                        <option>East</option>
                                        <option>East-South-East</option>
                                        <option>South East</option>
                                        <option>South-South-East</option>
                                        <option>South</option>
                                        <option>South-South-West</option>
                                        <option>South West</option>
                                        <option>West-South-West</option>
                                        <option>West</option>
                                        <option>West-North-West</option>
                                        <option>North West</option>
                                        <option>North-North-West</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="assessment_bldg_composition">Building Composition <span class="text-muted c-red" style="font-size:x-small ">Please Be Specific</span><span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="assessment_bldg_composition" placeholder="What is Burning Inside?">                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Essential Information/s</h4>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="owner_last_name">Owner's Last Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="owner_last_name" placeholder="Cruz">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="owner_first_name">Owner's First Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="owner_first_name" placeholder="Robert">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="owner_middle_name">Owner's Middle Name<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="owner_middle_name" placeholder="De La">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="owner_contact">Owner's Contact Number<span class="c-red"> *</span></label>
                                    <input type="integer" class="form-control" id="owner_contact" placeholder="XXX-XXX-XXXX">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="owner_email">Owner's Email<span class="c-red"> *</span></label>
                                    <input type="email" class="form-control" id="owner_email" placeholder="me@here.com">
                                <!--    <textarea class="form-control" rows="3"></textarea>-->
                                </div>
                            </div>
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="owner_address">Owner's Address<span class="c-red"> *</span></label>
                                    <input type="text" class="form-control" id="owner_address" placeholder="Blk 7. Village Name Street Name. City, Region">
                                </div>
                            </div>
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="assessment_injuries">Any Injuries?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="injuriesRadioOptions" id="injuriesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="" id="it">
                                    <table class="table table-bordered table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Middle Name</th>
                                                <th>Injury Obtained</th>
                                                <th width="5">
                                                    <button id="ra" type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="rt">
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name=" " value="" />
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <!--If Yes, Drop down an ADDITIONAL FIELDS of Casualties Information -->

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_casualties">Any Casualties?<span class="c-red"> *</span></label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions" id="casualtiesRadio1" value="Yes">Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="casualtiesRadioOptions"  id="casualtiesRadio2" value="No">No
                                    </label>                  
                                </div>
                                <div class="hidden"> <!--If Yes, Drop down an ADDITIONAL FIELDS of Casualties Information --></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_affected_building">Number of Structure/s Affected<span class="c-red"> *</span></label>
                                    <input type="number" class="form-control" id="assessment_affected_building" placeholder="2">                                
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label for="assessment_fire_type">Type of Fire<span class="c-red"> *</span></label>
                                    <select class="form-control" name="fire_type" id="assessment_fire_type">
                                        <option>Ordinary, Combustible Materials</option>
                                        <option>Flammable, Liquid and Gas</option>
                                        <option>Electrical</option>
                                        <option>Cooking Oils and Fats</option>
                                        <option>Metal</option>    
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_cause_fire">Cause of Fire <span class="text-muted c-red" style="font-size:x-small ">Accident or Intentional</span></label>
                                    <input type="text" class="form-control" id="assessment_cause_fire" placeholder="Why did it start?">                                    
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="assessment_fire_origin">Origin of Fire</label>
                                    <input type="text" class="form-control" id="assessment_fire_origin" placeholder="Where did it start?">                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="assessment_fire_source">Additional Info</label>
                                    <input type="text" class="form-control" id="assessment_fire_source" placeholder="Anything Else?">                                    
                                </div>
                            </div>
                        </div>
                        <br>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="    Revert    ">                            
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submit" value="   Save Only   ">
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="submit" name="submit" value="Broadcast and Save">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    <table>
        <thead>

        </thead>
        <tbody id="addrr">
            <tr>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <input class="form-control" type="text" name=" " value="" />
                </td>
                <td>
                    <button type="button" class="btn btn-xs btn-danger rr"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
        </tbody>
    </table>

</div>
<script>
    $("#it").hide();
    $("input").click(function () {
        var name = $(this).attr('name');
        var value = $(this).attr('value');
        if (name == "injuriesRadioOptions" && value == "Yes"){
            $("#it").show();
        }
        else {
            $("#it").hide();
        };

    });

    $("#ra").on("click", function () {
        $("#rt").append($("#addrr").html());
    });
    
    $("body").on("click", ".rr",function () {
        $(this).parent().parent().remove();
    });
</script>


<?php include_once './view/template/footer.php'; ?>