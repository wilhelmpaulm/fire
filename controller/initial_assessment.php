
<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
        <?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Pre Risk-Assessment Page</h1>


            <!--            <h2 class="sub-header">Section title</h2>-->
            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <!--                        <div class="form-group">
                                                    <label for="addressLocation">Incident Location<span class="c-red"> *</span></label>
                                                    <p class="help-block">Please be <span class="c-red">SPECIFIC</span> as possible</p>
                                                    <input type="text" class="form-control" id="addressLocation" placeholder="Blk 7. Village Name Street Name." autofocus="true">
                                                </div>-->
                        <!--                            <p> Barangay, City Region </p>-->
                        <div class="row">
                            <!-- Using (Alt + 255) Character Space-->
                            <h4 class="sub-header">   Identifiers</h4>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Fire ID</label>
                                    <input type="integer" class="form-control" placeholder="123456789" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-8">                             
                                <div class="form-group">
                                    <label>Incident Location</label>
                                    <input type="text" class="form-control" placeholder="Blk 7. Village Name Street Name. Barangay City, Region" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-3">                             
                                <div class="form-group">
                                    <label>Alarm Status</label>
                                    <input type="text" class="form-control" placeholder="5th Alarm" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Date Reported</label>
                                    <input type="datetime" class="form-control" placeholder="31 December 2014 18:30:00" disabled="true">
                                </div>
                            </div>
                            <div class="col-lg-4">                             
                                <div class="form-group">
                                    <label>Confirmed By</label>
                                    <input type="text" class="form-control" placeholder="Mario and Luigi" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="sub-header">   Initial Information/s</h4>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="respond_oic">Current Officer-In-Charge</label>
                                    <input type="text" class="form-control" id="respond_oic" placeholder="Cruz, Joseph De La" autofocus="true">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="respond_bldg_structure">Building Structure<span class="c-red"> *</span></label>
                                    <select class="form-control" name="building_structure" id="respond_bldg_structure">
                                        <option>Non-Residential</option>
                                        <option>Residential</option>
                                        <option>Commercial</option>
                                        <option>Educational</option>
                                        <option>Government</option>
                                        <option>Industrial</option>
                                        <option>Agricultural</option>
                                        <option>Religious</option>
                                        <option>Parking/Storage</option>
                                        <option>Transportaion</option>
                                        <option>Infrastructure</option>
                                        <option>Power Stations</option>
                                        <option>Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="respond_bldg_elevation">Elevation</label>
                                    <input type="text" class="form-control" id="respond_bldg_elevation" placeholder="What Floor?">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="respond_alarm_status">Reported Alarm Status</label>
                                    <select class="form-control" name="alarm_status" id="respond_alarm_status">
                                        <!--<option>NOT APPLICABLE</option>-->
                                        <option>5th-Alarm</option>
                                        <option>4th-Alarm</option>
                                        <option>3rd-Alarm</option>
                                        <option>2nd-Alarm</option>
                                        <option>1st-Alarm</option>
                                        <option>Task Force Alpha</option>
                                        <option>Task Force Bravo</option>
                                        <option>Task Force Charlie</option>
                                        <option>Task Force Delta</option>
                                        <option>Task Force Echo</option>
                                        <option>General Alarm</option>
                                    </select>                          
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="respond_wind_speed">Wind Speed <span class="text-muted c-red" style="font-size:x-small ">in kph</span></label>
                                    <input type="integer" class="form-control" id="respond_wind_speed" placeholder="6 kph">                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="respond_wind_direction">Wind Direction<span class="c-red"> *</span></label>
                                    <select class="form-control" name="wind_direction" id="respond_wind_direction">
                                        <option>North</option>
                                        <option>North-North-East</option>
                                        <option>North East</option>
                                        <option>East-North-East</option>
                                        <option>East</option>
                                        <option>East-South-East</option>
                                        <option>South East</option>
                                        <option>South-South-East</option>
                                        <option>South</option>
                                        <option>South-South-West</option>
                                        <option>South West</option>
                                        <option>West-South-West</option>
                                        <option>West</option>
                                        <option>West-North-West</option>
                                        <option>North West</option>
                                        <option>North-North-West</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="respond_bldg_composition">Building Composition <span class="text-muted c-red" style="font-size:x-small ">Please Be Specific</span></label>
                                    <input type="text" class="form-control" id="respond_bldg_composition" placeholder="What is Burning Inside?">                                    
                                </div>
                            </div>
                        </div>
                        <br>
                        <p><span class="c-red fs20"> * </span> -- required fields</p>
                        <input style="margin-left: 20px;" class="btn btn-primary pull-right" type="reset" name="revert" value="    Revert    ">                            
                            <input class="btn btn-primary pull-right" type="submit" name="submit" value="  Broadcast  ">                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once './view/template/footer.php'; ?>