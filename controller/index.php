<!--  Start of Coding -->
<?php
//Begin Initlization
//Connect to DB
$servername = "localhost";
$username = "root";
$password = "";

//Create Connection
$conn = mysqli_connect($servername, $username, $password, "pnp");
//$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


//var_dump(mysqli_query($conn, $sql));
//die();
//Select Data
$sql = "SELECT * FROM users";
$result = mysqli_query($conn, $sql);
?>
<!--  End of Coding -->




<!--    ////////////////////////////////////
        /////// this is thew header  ///////
        ////////////////////////////////////-->
<?php include_once './view/template/header.php'; ?>
<?php include_once './view/topbar/manager.php'; ?>
<!--    ////////////////////////////////////
        ///////    end of header     ///////
        ////////////////////////////////////-->


<div class="container-fluid">
    <div class="row">
        <!--    ////////////////////////////////////
                /////// this is thew sidebar ///////
                ////////////////////////////////////-->
<?php include_once './view/sidebar/manager.php'; ?>
        <!--    ////////////////////////////////////
                ///////   end for sidebar    ///////
                ////////////////////////////////////-->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <!--            <div class="row placeholders">
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                            <div class="col-xs-6 col-sm-3 placeholder">
                                <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
                                <h4>Label</h4>
                                <span class="text-muted">Something else</span>
                            </div>
                        </div>-->

            <h2 class="sub-header">Section title</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>                            
                            <th>ID</th>
                            <th>User Name</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach ($result as $r) { ?>      
                            <tr>                                
                                <td><?= $r['id']; ?></td>
                                <td><?= $r['username']; ?></td>
                                <td><?= $r['last_name']; ?></td>
                                <td><?= $r['first_name']; ?></td>
                                <td><?= $r['middle_name']; ?></td>
                            </tr>
                            <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php include_once './view/template/footer.php'; ?>