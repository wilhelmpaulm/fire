<?php

include './lib/AltoRouter.php';

$router = new AltoRouter();
# $router->setBasePath('');
//var_dump($db->select("barangays", "*"));
###################################################################
###################################################################
####################################################################
#  file method
#  $router->map('GET', '/', './view/index.php' );
#
#  function method
#$router->map('GET', '/', function() {
#    include './view/index.php';
#});
#
###################################################################
#
#    include './lib/config.php';
#    var_dump($db->select("barangays", "*"));

$router->map('GET', '/', function() {
    include './view/index.php';
});

$router->map('GET', '/report', function() {
    include './controller/report_fire.php';
});

$router->map('GET', '/activation', function() {
    include './controller/confirm_Fire.php';
});

$router->map('GET', '/response', function() {
    include './controller/initial_assessment.php';
});

$router->map('GET', '/edit', function() {
    include './controller/risk_assessment.php';
});

$router->map('GET', '/recovery', function() {
    include './controller/post_response.php';
});

$router->map('GET', '/list', function() {
    include './controller/list.php';
});

$router->map('GET', '/login', function() {
    include './view/login.php';
});

$router->map('POST', '/login', function() {
    #action here
});

$router->map('POST', '/logout', function() {

    #action here
});



###################################################################
###################################################################
###################################################################

$match = $router->match();

if ($match && is_callable($match['target'])) {
    call_user_func_array($match['target'], $match['params']);
} else if ($match) {
    require $match['target'];
} else {
    header("HTTP/1.0 404 Not Found");
    require '404.php';
}
// Special (payments, ajax processing, etc)
//$router->map('GET', '/charge/[*:customer_id]/', 'charge.php', 'charge');
//$router->map('GET', '/pay/[*:status]/', 'payment_results.php', 'payment-results');
// API Routes
//$router->map('GET', '/api/[*:key]/[*:name]/', 'json.php', 'api');

