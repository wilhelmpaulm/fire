<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-thumbs-up"></i> Approval</a></li>
        <li><a href="#"><i class="fa fa-book"></i>  Reports</a></li>
        <li><a href="#"><i class="fa fa-list-alt"></i> Analytics</a></li>
        <li><a href="#"><i class="fa fa-truck"></i> Equipment Credentials</a></li>
    </ul>

</div>